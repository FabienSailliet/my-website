import React, { useState } from "react";
import { Box, Typography } from "@material-ui/core";
import TagsFilter from "./TagsFilter";
import CardsGrid from "./CardsGrid";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    root: {
        "& > *:not(:last-child)": {
            marginBottom: theme.spacing(4)
        }
    },
    sectionHeader: {
        marginBottom: theme.spacing(2)
    }
}));

function FilterableCards(props) {
    const classes = useStyles();

    const [filterTags, setFilterTags] = useState([]);

    const { xs, sm, md, lg, xl } = props;

    function handleTagClick(tag) {
        const newTags = filterTags.slice();

        if (!newTags.map((tag) => tag.name).includes(tag.name)) {
            newTags.push(tag);

            setFilterTags(newTags);
        }
    }

    function isCardToBeDisplayed(card) {
        const cardTagNames = card.tags.map((tag) => tag.name);

        for (let filterTagName of filterTags.map((tag) => tag.name)) {
            if (!cardTagNames.includes(filterTagName)) {
                return false;
            }
        }
        return true;
    }

    let cards;
    if (filterTags.length === 0) {
        cards = props.sections.map(section => 
            <Box key={section.name}>
                {section.name !== null &&
                    <Typography component="h2" variant="h5" className={classes.sectionHeader}>
                        {section.name}
                    </Typography>
                }
                <CardsGrid
                    cards={section.cards}
                    onTagClick={handleTagClick}
                    {...{xs, sm, md, lg, xl}}
                />
            </Box>
        )
    }
    else {
        let allCards = props.sections.map(section => section.cards).flat();

        cards = (
            <CardsGrid
                cards={allCards.filter(isCardToBeDisplayed)}
                onTagClick={handleTagClick}
                {...{xs, sm, md, lg, xl}}
            />
        )
    }

    return (
        <Box className={classes.root}>
            <TagsFilter tags={filterTags} onTagsChanged={setFilterTags} />
            {cards}
        </Box>
    );
}

const validColsNb = [1, 2, 3, 4, 6, 12]

FilterableCards.propTypes = {
    sections: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        cards: PropTypes.arrayOf(PropTypes.shape({
            title: PropTypes.node,
            subheader: PropTypes.node,
            description: PropTypes.node,
            avatar: PropTypes.node,
            tags: PropTypes.array,
            actions: PropTypes.array
        }))
    })),
    xs: PropTypes.oneOf(validColsNb),
    sm: PropTypes.oneOf(validColsNb),
    md: PropTypes.oneOf(validColsNb),
    lg: PropTypes.oneOf(validColsNb),
    xl: PropTypes.oneOf(validColsNb)
}

export default FilterableCards;