import React from "react";
import { makeStyles } from "@material-ui/styles";
import PropTypes from "prop-types";
import { Box, Chip } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        margin: -theme.spacing(0.5),
        "& > *": {
            margin: theme.spacing(0.5)
        }
    }
}));

function TagsList(props) {
    const classes = useStyles();

    return (
        <Box className={classes.root}>
            {props.tags.map((tag) =>
                <Chip
                    label={tag.name}
                    color={tag.color}
                    onClick={(props.onTagClick) ?
                        () => props.onTagClick(tag) :
                        undefined
                    }
                    onDelete={(props.onTagDelete) ?
                        () => props.onTagDelete(tag) :
                        undefined
                    }
                    key={tag.name}
                />
            )}
        </Box>
    );
}

TagsList.propTypes = {
    tags: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string.isRequired,
        color: PropTypes.string
    })),
    onTagClick: PropTypes.func,
    onTagDelete: PropTypes.func
}

export default TagsList;