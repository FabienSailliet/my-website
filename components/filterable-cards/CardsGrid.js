import React from 'react';
import { Grid } from "@material-ui/core";
import TaggedCard from './TaggedCard';

function CardsGrid(props) {
    const { xs, sm, md, lg, xl } = props;

    return (
        <Grid container spacing={3}>
            {props.cards.map((card) =>
                <Grid item {...{xs, sm, md, lg, xl}} key={`${card.title}${card.subheader}`}>
                    <TaggedCard {...card} onTagClick={props.onTagClick} />
                </Grid>
            )}
        </Grid>
    )
}

export default CardsGrid;