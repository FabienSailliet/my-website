import React from 'react';
import { Card, CardHeader, CardContent, Typography, CardActions, Button } from "@material-ui/core";
import TagsList from './TagsList';

const tagsColorPriority = ["primary", "secondary", "default"];

function TaggedCard(props) {
    const tags = props.tags.slice().sort((t1, t2) => {
        if (t1.color === t2.color) {
            return t1.name.localeCompare(t2.name);
        }
        else {
            return (tagsColorPriority.indexOf(t1.color) < tagsColorPriority.indexOf(t2.color)) ? -1 : 1;
        }
    });

    return (
        <Card>
            <CardHeader
                title={props.title}
                subheader={props.subheader}
                avatar={props.avatar}
            />
            <CardContent>
                <Typography variant="body2" color="textSecondary" paragraph>
                    {props.description}
                </Typography>
                <TagsList tags={tags} onTagClick={props.onTagClick} />
            </CardContent>
            {props.actions &&
                <CardActions>
                    {props.actions.map((action) =>
                        <Button {...action} key={action.children} />
                    )}
                </CardActions>
            }
        </Card>
    );
}

export default TaggedCard;