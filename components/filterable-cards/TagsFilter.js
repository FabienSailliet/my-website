import React from "react";
import TagsList from "./TagsList";
import { Box, Typography, Chip, Link } from "@material-ui/core";
import PropTypes from "prop-types";
import AddIcon from '@material-ui/icons/Add';
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    titleRow: {
        display: "flex",
        alignItems: "center",
        "& > *:not(:last-child)": {
            marginRight: theme.spacing(4)
        },
        "& > a": {
            cursor: "pointer"
        }
    }
}));

function TagsFilter(props) {
    const classes = useStyles();

    function deleteTag(tag) {
        props.onTagsChanged(props.tags.filter(tag2 => tag2.name != tag.name));
    }

    return (
        <Box>
            <Box className={classes.titleRow}>
                <Typography variant="subtitle1">Filters</Typography>
                {(props.tags.length > 0) &&
                    <Link onClick={() => props.onTagsChanged([])}>Clear</Link>
                }
            </Box>

            {(props.tags.length === 0) ?
                <Chip label="Click on a tag to add it to the filters" avatar={<AddIcon />} disabled /> :
                <TagsList tags={props.tags} onTagDelete={deleteTag} />
            }
        </Box>
    )
}

TagsFilter.propTypes = {
    tags: PropTypes.array.isRequired,
    onTagsChanged: PropTypes.func.isRequired
}

export default TagsFilter;