import React from "react";
import { makeStyles } from "@material-ui/styles";
import { Container } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    root: {
        paddingTop: theme.spacing(2),
        paddingBottom: theme.spacing(2)
    }
}));

function PageContainer(props) {
    const classes = useStyles();

    return (
        <Container className={classes.root}>
            {props.children}
        </Container>
    );
}

export default PageContainer;