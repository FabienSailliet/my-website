import React, { useState } from "react";
import Cookies from "universal-cookie";
import { blue, pink } from "@material-ui/core/colors";
import { createMuiTheme, ThemeProvider, CssBaseline } from "@material-ui/core";
import App from "./App";

function createTheme(isDark) {
    return {
        palette: {
            type: (isDark) ? "dark" : "light",
            primary: blue,
            secondary: pink,
        }
    };
}

function updateThemeCookie(themeBase) {
    cookies.set(
        "theme",
        {
            theme: themeBase,
            uid: THEME_COOKIE_UID
        },
        {
            path: "/",
            maxAge: 31536000 // one year
        }
    );
}

const cookies = new Cookies();
const THEME_COOKIE_UID = 1;

let themeBase;
let themeCookie = cookies.get("theme");
if (!themeCookie || themeCookie.uid !== THEME_COOKIE_UID) {
    themeBase = createTheme(true);
    updateThemeCookie(themeBase);
}
else {
    themeBase = themeCookie.theme;
}

const loadedTheme = createMuiTheme(themeBase);

export function withLayout(Page, title) {
    return () => {
        const [ theme, setTheme ] = useState(loadedTheme);

        function handleDarkThemeSwitch() {
            const newThemeBase = createTheme(theme.palette.type !== "dark");

            setTheme(createMuiTheme(newThemeBase));

            updateThemeCookie(newThemeBase);

            // Fix the theme issue with client-side routing
            location.reload();
        }

        return (
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <App onDarkThemeSwitch={handleDarkThemeSwitch} page={<Page />} title={title} />
            </ThemeProvider>
        );
    }
}