import React from "react"
import { Box, Drawer, List, ListItem, ListItemIcon, ListItemText, Divider, Typography, Hidden, SwipeableDrawer, Switch, ListItemSecondaryAction } from "@material-ui/core"
import { makeStyles } from "@material-ui/styles";
import Brightness3Icon from '@material-ui/icons/Brightness3';
import IconLinks from "./IconLinks";
import { links, general } from "../../model/model";
import Link from "next/link"
import { useRouter } from 'next/router'

const drawerWidth = 250;

const useStyles = makeStyles((theme) => ({
    title: {
        textAlign: "center"
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0
    },
    drawerContent: {
        height: "100%",
        width: drawerWidth,
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between"
    },
    activeDrawerButton: {
        borderLeft: `5px solid ${theme.palette.primary.main}`
    },
    drawerHeader: {
        margin: `${theme.spacing(2)}px 0`
    },
    devVersionNotification: {
        backgroundColor: theme.palette.error.main,
        padding: theme.spacing(1)
    }
}));

class ListLink extends React.Component {
    render() {
        return (
            <Link href={this.props.href}>
                <a className={this.props.className}>
                    {this.props.children}
                </a>
            </Link>
        );
    }
}

function ResponsiveDrawer(props) {
    const classes = useStyles();
    const router = useRouter();

    const drawerBody = (
        <Box>
            <Box className={classes.drawerHeader}>
                <Typography variant="h4" component="h1" className={classes.title}>{general.siteName}</Typography>
                <IconLinks links={links} className={classes.iconLinks} />
            </Box>
            <Divider />
            <List>
                {props.pages.map((link) =>
                    <ListItem
                        button
                        component={ListLink}
                        href={link.link}
                        key={link.name}
                        className={(router.pathname === link.link) ? classes.activeDrawerButton : undefined}
                    >
                        <ListItemIcon>{link.icon}</ListItemIcon>
                        <ListItemText>{link.name}</ListItemText>
                    </ListItem>
                )}
            </List>
        </Box >
    );

    const drawerFooter = (
        <Box>
            <Divider />
            <List>
                <ListItem>
                    <ListItemIcon><Brightness3Icon /></ListItemIcon>
                    <ListItemText>Dark Theme</ListItemText>
                    <ListItemSecondaryAction>
                        <Switch color="primary" edge="end" checked={props.darkTheme} onChange={props.onDarkThemeSwitch} />
                    </ListItemSecondaryAction>
                </ListItem>
            </List>
            {props.version.includes("+dev") &&
                <Typography className={classes.devVersionNotification} align="center">
                    Development Version
                </Typography>
            }
        </Box>
    );

    const drawerContent = (
        <Box className={classes.drawerContent}>
            {drawerBody}
            {drawerFooter}
        </Box>
    );

    return (
        <Box>
            <Hidden xsDown>
                <Drawer variant="permanent" className={classes.drawer}>
                    {drawerContent}
                </Drawer>
            </Hidden>
            <Hidden smUp>
                <SwipeableDrawer
                    variant="temporary"
                    open={props.open}
                    onOpen={props.onOpen}
                    onClose={props.onClose}
                    ModalProps={{
                        keepMounted: true
                    }}
                    className={classes.drawer}
                >
                    {drawerContent}
                </SwipeableDrawer>
            </Hidden>
        </Box>
    )
}

export default ResponsiveDrawer;