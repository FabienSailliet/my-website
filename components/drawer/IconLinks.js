import React from "react";
import { Box, Avatar, Icon, Link } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";

const useStyles = makeStyles((theme) => ({
    linksList: {
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
        "& > *": {
            margin: theme.spacing(1)
        }
    }
}));

function IconLinks (props) {
    const classes = useStyles();

    return (
        <Box className={classes.linksList + " " + props.className}>
            {props.links.map(link =>
                <Link href={link.href} target="_blank" rel="noopener" underline="none" key={link.name}>
                    <Avatar alt={link.name} src={link.logo} title={link.name}>
                        <Icon>{link.iconName}</Icon>
                    </Avatar>
                </Link>
            )}
        </Box>
    );
}

export default IconLinks;