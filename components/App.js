import React, { useState } from 'react';
import { Box, AppBar, Toolbar, IconButton, Hidden, useTheme } from '@material-ui/core';
import CodeIcon from '@material-ui/icons/Code';
import HistoryIcon from '@material-ui/icons/History';
import HomeIcon from '@material-ui/icons/Home';
import MenuIcon from '@material-ui/icons/Menu';
import SettingsIcon from '@material-ui/icons/Settings';
import { makeStyles } from '@material-ui/styles';
import ResponsiveDrawer from './drawer/ResponsiveDrawer';
import Head from "next/head"
import { general } from '../model/model';

const version = require("../version");

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        height: "100%"
    },
    content: {
        display: "flex",
        flexDirection: "column",
        flexGrow: 1,
    },
    toolbar: theme.mixins.toolbar
}));

const pages = [
    { name: "Home", link: "/", icon: <HomeIcon /> },
    { name: "History", link: "/history", icon: <HistoryIcon /> },
    { name: "Projects", link: "/projects", icon: <CodeIcon /> },
    { name: "Skills", link: "/skills", icon: <SettingsIcon /> },
];

function App(props) {
    const classes = useStyles();
    const theme = useTheme();

    const [ drawerIsOpen, setDrawerIsOpen ] = useState(false);

    function handleDrawerButtonClick() {
        setDrawerIsOpen(!drawerIsOpen);
    }

    function handleDrawerOpen() {
        setDrawerIsOpen(true);
    }

    function handleDrawerClose() {
        setDrawerIsOpen(false);
    }

    return (
        <Box className={classes.root}>
            <Head>
                <title>
                    {(props.title) ?
                        `${props.title} - ${general.siteName}` :
                        general.siteName
                    }
                </title>
            </Head>
            <nav>
                <ResponsiveDrawer
                    pages={pages}
                    open={drawerIsOpen}
                    onOpen={handleDrawerOpen}
                    onClose={handleDrawerClose}
                    darkTheme={theme.palette.type === "dark"}
                    onDarkThemeSwitch={props.onDarkThemeSwitch}
                    version={version}
                />
            </nav>
            <main className={classes.content}>
                <Hidden smUp>
                    <AppBar color="default">
                        <Toolbar>
                            <IconButton edge="start" onClick={handleDrawerButtonClick}>
                                <MenuIcon />
                            </IconButton>
                        </Toolbar>
                    </AppBar>
                    <div className={classes.toolbar} />
                </Hidden>
                {props.page}
            </main>
        </Box>
    );
}

export default App;
