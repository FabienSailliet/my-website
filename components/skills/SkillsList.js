import React from "react";
import { Box, List } from "@material-ui/core";
import Skill from "./Skill";
import { makeStyles } from "@material-ui/styles";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
    skillsIconsList: {
        display: "flex",
        justifyContent: "center",
        flexWrap: "wrap",
        margin: -theme.spacing(1),
        "& > *": {
            margin: theme.spacing(1)
        }
    },
    skillsList: {
        padding: 0
    }
}));

function SkillsList(props) {
    const classes = useStyles();

    return (props.variant === "icons") ?
        (
            <Box className={classes.skillsIconsList}>
                {props.skills.map((skill) =>
                    <Skill {...skill} variant="avatar" key={skill.name} />
                )}
            </Box>
        ) :
        (
            <List className={classes.skillsList}>
                {props.skills.map((skill) =>
                    <Skill {...skill} variant="listItem" key={skill.name} />
                )}
            </List>
        );
}

SkillsList.propTypes = {
    variant: PropTypes.oneOf(["icons", "iconsAndNames"]).isRequired,
    skills: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        logo: PropTypes.string
    }))
}

export default SkillsList;