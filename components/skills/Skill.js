import React from "react";
import { Avatar, ListItem, ListItemText, ListItemAvatar } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import PropTypes from "prop-types";

const avatarSize = 50;

const useStyles = makeStyles((theme) => ({
    avatar: {
        height: avatarSize,
        width: avatarSize
    }
}));

function Skill(props) {
    const classes = useStyles();

    return (props.variant === "avatar") ?
        (
            <Avatar alt={props.name} src={props.logo} title={props.name} className={classes.avatar} />
        ) :
        (
            <ListItem>
                <ListItemAvatar>
                    <Avatar alt={props.name} src={props.logo} title={props.name} />
                </ListItemAvatar>
                <ListItemText>{props.name}</ListItemText>
            </ListItem>
        );
}

Skill.propTypes = {
    variant: PropTypes.oneOf(["avatar", "listItem"]).isRequired,
    name: PropTypes.string,
    logo: PropTypes.string
}

export default Skill;