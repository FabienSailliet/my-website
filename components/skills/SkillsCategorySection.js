import React from "react";
import { Box, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import SkillsList from "./SkillsList";
import PropTypes from "prop-types";

const useStyles = makeStyles((theme) => ({
    section: {
        padding: theme.spacing(2),
        [theme.breakpoints.down("sm")]: {
            padding: `${theme.spacing(2)}px 0`
        }
    },
    title: {
        textAlign: "center",
        marginBottom: theme.spacing(2)
    }
}));

function SkillsCategorySection(props) {
    const classes = useStyles();

    return (
        <Box className={classes.section}>
            <Typography variant="h5" component="h3" className={classes.title}>{props.name}</Typography>
            <SkillsList skills={props.skills} variant={props.variant} />
        </Box>
    )
}

SkillsCategorySection.propTypes = {
    variant: PropTypes.oneOf(["icons", "iconsAndNames"]).isRequired,
    skills: PropTypes.arrayOf(PropTypes.shape({
        name: PropTypes.string,
        logo: PropTypes.string
    }))
}

export default SkillsCategorySection;