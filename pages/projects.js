import React from "react"
import { projects } from '../model/model';
import { Avatar, useMediaQuery, useTheme } from "@material-ui/core";
import FilterableCards from "../components/filterable-cards/FilterableCards";
import OpenInNewIcon from '@material-ui/icons/OpenInNew';
import PageContainer from "../components/PageContainer";
import { withLayout } from "../components/Layout";

function ProjectsPage() {
    const theme = useTheme();

    const breakpointsUp = ["xs", "sm", "md", "lg", "xl"].reduce(
        (breakpoints, breakpoint) => {
            breakpoints[breakpoint] = useMediaQuery(theme.breakpoints.up(breakpoint));
            return breakpoints;
        },
        {}
    );

    const topProjectsNbBreakpoints = {
        xl: 4,
        lg: 3,
        md: 4,
        xm: 3
    }
    let topProjectsNb = 4;
    for (const [breakpoint, cardsNb] of Object.entries(topProjectsNbBreakpoints)) {
        if (breakpointsUp[breakpoint]) {
            topProjectsNb = cardsNb;
            break;
        }
    }

    const cards = projects.map((project) => ({
        title: project.name,
        description: project.description,
        avatar: <Avatar src={project.logo} alt={project.name} />,
        tags: project.tags.map((tag) => ({
            ...tag,
            color:
                (tag.category === "Project types") ?
                    "primary" :
                    (tag.category === "Languages") ?
                        "secondary" :
                        "default"
        })),
        actions: [
            {
                children: "Source Code",
                target: "_blank",
                href: project.sourceUrl,
                rel: "noopener",
                startIcon: <OpenInNewIcon />
            }
        ]
    }));
    cards.sort((card1, card2) => card1.importance - card2.importance);

    const sections = [
        {name: "Main Projects", cards: cards.slice(0, topProjectsNb)},
        {name: "Other Projects", cards: cards.slice(topProjectsNb)}
    ];

    return (
        <PageContainer>
            <FilterableCards xs={12} md={6} lg={4} xl={3} sections={sections} />
        </PageContainer>
    )

}

export default withLayout(ProjectsPage, "Projects");