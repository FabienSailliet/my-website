// Took from https://github.com/mui-org/material-ui/tree/master/examples/nextjs

import React from 'react';
import App from 'next/app';
import Head from 'next/head';
import { ThemeProvider, createGenerateClassName } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import theme from '../src/theme';
import { general } from "../model/model"

console.info("Version: " + require("../version.json"));

export default class MyApp extends App {
  componentDidMount() {
    // Remove the server-side injected CSS.
    const jssStyles = document.querySelector('#jss-server-side');
    if (jssStyles) {
      jssStyles.parentElement.removeChild(jssStyles);
    }
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <React.Fragment>
        <Head>
          <title>{general.siteName}</title>
          <meta name="description" content={general.description} />
          <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
          
          <style>{`
            html, body, #__next {
              height: 100%;
            }
          `}</style>
        </Head>
        <ThemeProvider theme={theme}>
          {/* CssBaseline kickstart an elegant, consistent, and simple baseline to build upon. */}
          <CssBaseline />
          <Component {...pageProps} />
        </ThemeProvider>
      </React.Fragment>
    );
  }
}
