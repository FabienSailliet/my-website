import React from "react";
import { Box, Typography, Container, Button } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { homepage } from "../model/model";
import Link from "next/link"
import { withLayout } from "../components/Layout";

const useStyles = makeStyles((theme) => ({
    homePage: {
        display: "flex",
        height: "100%",
        width: "100%",
        backgroundImage: `url(/img/homepage/background.jpg)`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        color: "#ffffff"
    },
    container: {
        margin: "auto",
        textAlign: "center"
    }
}));

function HomePage(props) {
    const classes = useStyles();

    return (
        <Box className={classes.homePage}>
            <Container maxWidth="sm" className={classes.container}>
                <Typography variant="h3" component="p" paragraph>{homepage.title}</Typography>
                <Typography paragraph>{homepage.body}</Typography>
                <Link href="/projects">
                    <Button variant="contained" color="primary" href="/projects">See Projects</Button>
                </Link>
            </Container>
        </Box>
    );
}

export default withLayout(HomePage, "Home");