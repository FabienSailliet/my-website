import React, { useState } from "react";
import { Grid, FormGroup, FormControlLabel, Checkbox } from "@material-ui/core";
import { skills } from "../model/model";
import SkillsCategorySection from "../components/skills/SkillsCategorySection";
import PageContainer from "../components/PageContainer";
import { makeStyles } from "@material-ui/styles";
import { withLayout } from "../components/Layout";

const useStyles = makeStyles((theme) => ({
    options: {
        justifyContent: "center"
    }
}));

function SkillsPage(props) {
    const classes = useStyles();

    const [showLabels, setShowLabels] = useState(false);

    return (
        <PageContainer>
            <FormGroup row className={classes.options}>
                <FormControlLabel
                    control={
                        <Checkbox color="primary" checked={showLabels} onChange={(event) => setShowLabels(event.target.checked)} />
                    }
                    label="Show labels"
                />
            </FormGroup>
            <Grid container spacing={3}>
                {skills.map((category) =>
                    <Grid item xs={12} md={6} lg={4} key={category.name}>
                        <SkillsCategorySection {...category} variant={(showLabels) ? "iconsAndNames" : "icons"} />
                    </Grid>
                )}
            </Grid>
        </PageContainer>
    )
}

export default withLayout(SkillsPage, "Skills");