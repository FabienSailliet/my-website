import React from "react";
import { Avatar } from "@material-ui/core";
import { history } from "../model/model";
import FilterableCards from "../components/filterable-cards/FilterableCards";
import WorkIcon from '@material-ui/icons/Work';
import SchoolIcon from '@material-ui/icons/School';
import { makeStyles } from "@material-ui/styles";
import PageContainer from "../components/PageContainer";
import { withLayout } from "../components/Layout";

const useStyles = makeStyles((theme) => ({
    eventName: {
        fontWeight: "bold"
    }
}));

function HistoryPage(props) {
    const classes = useStyles();

    const cards = history.map((event) => ({
        title: (
            <span>
                <span className={classes.eventName}>{event.name}</span>
                {event.location &&
                    <span> at {event.location}</span>
                }
            </span>
        ),
        subheader: event.period,
        description: event.description,
        avatar: (
            <Avatar src={event.logo} alt={event.name}>
                {(event.tags.some(tag => tag.category === "Types" && tag.name === "Work")) ?
                    <WorkIcon /> :
                    <SchoolIcon />
                }
            </Avatar>
        ),
        tags: event.tags.map((tag) => ({
            ...tag,
            color:
                (tag.category === "Types") ?
                    "primary" :
                    (tag.category === "Languages") ?
                        "secondary" :
                        "default"
        }))
    }));

    const sections = [
        {name: null, cards: cards}
    ]

    return (
        <PageContainer>
            <FilterableCards sections={sections} xs={12} />
        </PageContainer>
    );
}

export default withLayout(HistoryPage, "History");