# Changelog

## 1.2.0 - 27/04/2022

### Added

- Docker image

### Changed

- Updated skills and history
- Updated NPM dependencies

## 1.1.0 - 09/02/2021

### Added

- Display event duration in the history page
- Display most important projects in a dedicated section

### Changed

- Updated dependencies

### Fixed

- The `See Projects` button on the home page was loading a new page from the server instead of seamlessly switching

## 1.0.0 - 29/01/2020

### Basic Features

- Presentation website made with React and Material-UI, using Next.js
- Server-side rendering
- Dark theme switch
- All the data is loaded from JSON files

### History & Projects

- The history and projects are displayed in cards that can be filtered based on assigned tags

### Skills

- Skills are displayed in icons
- Possible to display the skills' names next to the icon