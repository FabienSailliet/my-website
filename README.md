# My Website

This website is deployed at [fabien.sailliet.fr](https://fabien.sailliet.fr).

It is made with [React](https://reactjs.org/), [Material-UI](https://material-ui.com/) and [Next.js](https://nextjs.org/).

## Installation

To get the development environment ready, simply run in the project root directory:

```
npm install
```

## NPM Commands

### `npm run dev`

Starts the development server.

### `npm run build`

Creates an optimised production build of the website and generates the static pages in the `out/` directory.

### `npm start`

Run the optimised production build, which can be generated with `npm run build`.