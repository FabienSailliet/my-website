function getLogos(elements, dirName) {
    for (let element of elements) {
        if (element.logoName) {
            element.logo = `/img/${dirName}/${element.logoName}`
        }
    }
}

function transformTags(elements) {
    for (let element of elements) {
        element.tags = Object.keys(element.tags)
            .map((category) => element.tags[category].map((tagName) => ({name: tagName, category: category})))
            .flat();
    }
}

/** @type Object */
export const general = require("./general");

/** @type Object */
export const homepage = require("./homepage");

/** @type Array */
export const projects = require("./projects.json");
getLogos(projects, "projects-logos");
transformTags(projects);
for (let project of projects) {
    if (project.importance === undefined) {
        project.importance = 0;
    }
}

/** @type Array */
export const skills = require("./skills");
for (let category of skills) {
    getLogos(category.skills, "skills");
}

/** @type Array */
export const links = require("./links");
getLogos(links, "links");

/** @type Array */
export const history = require("./history");
getLogos(history, "history");
transformTags(history);

function dateToStr(date) {
    return date.toLocaleString("en-EN", {month: "short", year: "numeric"});
}

for (let event of history) {
    let period = event.period;
    let start = new Date(period.start);
    let end = period.end ? new Date(period.end) : new Date();
    
    if (!("duration" in period)) {
        let monthsStart = start.getFullYear() * 12 + start.getMonth();
        let monthsEnd = end.getFullYear() * 12 + end.getMonth();
        let months = monthsEnd - monthsStart;
        if (start.getDate() > end.getDate()) {
            months--;
        }
        if (months >= 12) {
            let years = Math.floor(months / 12);
            period.duration = `${years} year${years != 1 ? "s" : ""}`;
        }
        else {
            period.duration = `${months} month${months != 1 ? "s" : ""}`;
        }
    }

    event.period = `${dateToStr(start)} - ${period.end ? dateToStr(end) : "Present"} (${period.duration})`;
}