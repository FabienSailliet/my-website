import { createMuiTheme } from '@material-ui/core/styles';
import { red, blue, pink } from '@material-ui/core/colors';

// Create a theme instance.
const theme = createMuiTheme({
  palette: {
      type: "dark",
      primary: blue,
      secondary: pink,
  }
});

export default theme;
