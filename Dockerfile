FROM node:16 as build

WORKDIR /app

COPY ./package.json ./package-lock.json /app/

RUN npm install
COPY . .
RUN npm run build

FROM nginx

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /app/out /usr/share/nginx/html